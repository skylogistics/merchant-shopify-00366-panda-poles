import { setTimeout } from 'timers';
import SkyboxSDK from '@skyboxcheckout/merchant-sdk';

const _ = require('underscore');
const arrayDiff = require('simple-array-diff');
const tingle = require('tingle.js');
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]      
};    
const localStore = engine.createStore(storage.local, []);
const sessionStore = engine.createStore(storage.session, []);
// Local libs
const shopifyStyles = require('../../assets/css/shopify.css');
const __cnStore = require('../../config/store.json');
const api = require('../xhr');

window.Sdk = new SkyboxSDK({
  IDSTORE: __cnStore.IDSTORE,
  MERCHANT: __cnStore.MERCHANT,
  MERCHANTCODE: __cnStore.MERCHANTCODE,
  STORE_URL: __cnStore.STORE_URL,
  SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
  CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
  CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS,
  CHANGE_COUNTRY_TEXT_USA: __cnStore.CHANGE_COUNTRY_TEXT_USA,
  CHECKOUT_BUTTON_STYLE: {
    SHOW: 'sky-btn-show',
    HIDE: 'sky-btn-hide'
  }
});

$(document).ready(function () {
  if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1) {
    $('#skybox-checkout-change-country').remove(); 
    $('<div id="skybox-checkout-change-country"></div>')
    .insertAfter('.main_header');
    Sdk.Common().initChangeCountry();
    $('#skybox-checkout-change-country').show();
  } else { 
    $('#skybox-checkout-change-country').hide();
    Sdk.Common().initBtnSkyCheckout();
  }

  // Skybox Ckeckout page & Iframe Source
  if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1) {
    $('#skybox-international-checkout').insertBefore('#page');
    $('#free').hide();
    $('#page').hide();
    $('#text-page').removeAttr('id');
    $('#skybox-international-checkout').css('display', 'none');
    $('html').css('background-color', 'transparent');
    $('.count_number').hide();

    api.get('/cart.js?_=' + $.now()).then(function(cart) {
      let data = JSON.parse(cart.responseText);

      if (data.item_count === 0) {
        window.location.assign('/');
      }

      Sdk.deleteProductsCart().then(function() {
        synchronizeCart(cart);
      });
    });
  }
  // Skybox Ckeckout Successfull page & Invoice Source
  if ($(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) > -1) {
    $('#skybox-international-checkout-invoice').insertBefore('#page');
    $('#text-page').removeAttr('id');
    $('html').css('background-color', 'transparent');
    $('#free').hide();
    $('#page').hide();
    $('.count_number').hide();

    if ($('#skybox-international-checkout-invoice').length > 0) {
      let rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";

      if (__cnStore.SKBX_LOADER_NAME) {
        rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;
      }

      $('#skybox-international-checkout-invoice').html(`
        <h1 id="mensaje" style="text-align:center;">
          <img src="${rutaLoaderGif}"/>
        </h1>
      `);
      
      api.get(__cnStore.STORE_URL + '/cart/clear.js?_=' + $.now()).then(function() {
        Sdk.getCartInvoice().then(function (content) {
          var contentHTML = JSON.parse(content).Data.Invoice;
          var invoice = document.getElementById('skybox-international-checkout-invoice');
          invoice.innerHTML = contentHTML;
        });
      });
    }
  };


  function addProductItems(products) {
    var ListProducts = [];

    products.forEach(function (productItem) {
      let title = productItem.product_title;
      var getWeight = Sdk.calcWeightAndUnit(productItem.grams);

      if (__cnStore.CONF.PRODUCT_TYPE_DEFAULT !== '') {
        productItem.product_type = __cnStore.CONF.PRODUCT_TYPE_DEFAULT;
      }

      let SKU;

      if (!_.isUndefined(productItem.sku) && !_.isNull(productItem.sku) && productItem.sku !== "") {
        SKU = productItem.sku;
      }  else {
        SKU = 'RESTRICTED';
      }

      let _Optionals = {
        CustomFields : [
          String(SKU)
        ]
      }

      var variant_title = (_.isNull(productItem.variant_title))? '': '-'+productItem.variant_title;

      let Product = {
        HtmlObjectid: productItem.properties ? String(productItem.key) : String(productItem.variant_id),
        Id: 0,
        Sku: productItem.properties ? String(productItem.key) : String(productItem.variant_id),
        Name: title+variant_title,
        Category: productItem.product_type,
        Price: (productItem.price / 100),
        ImgUrl: productItem.image,
        Language: "",
        Weight: getWeight.weight,
        WeightUnit: getWeight.unit,
        VolumetricWeight: 0,
        DefinitionOpt: "",
        Quantity: productItem.quantity,
        variantMerchantId: String(productItem.variant_id),
        productMerchantId: String(productItem.product_id)
      }

      ListProducts.push(
        { "Product": Product,
          "Optionals": _Optionals
        }
      );
    });

    return { "ListProducts": ListProducts };
  }

  function validateRestriction(arraySky, arrayStore, currentCountry) {
    var clonedArrStore = arrayStore,
        arrListUpdate = [],
        objListUpdate ={},
        html = '';

    var existsProperties = false;
    clonedArrStore.map(function(e) {
      arrListUpdate.push(e.quantity);
    });

    var hasContent = false;

    if (_.isArray(arraySky) && arraySky.length > 0) {        
      html += '<div>';
      html += '<br />';
      html += ' <p class="restricted-msg">' + 'The following products will be removed from your shopping cart, they are' + '<strong>' + ' restricted in ' + currentCountry + '</strong>' + '</p>';
      html += '<br />';
      html += ' <table class="skbx-restricted-table">';
      html += '   <tbody>';

      arraySky.map(function(item) {
        for (var i in clonedArrStore) {
          var objStore = clonedArrStore[i];
          var properties = objStore.properties;
          var htmlProperties = '';

          if ((item.HtmlObjectId).trim() === String(objStore.Code).trim()) {
            if (objStore.grams === 0) {
              hasContent = true;
              var title = objStore.title;
              
              if (properties) {
                existsProperties = true;
                arrListUpdate[i] = 0;

                for (var name in properties) {
                  htmlProperties += name + ': ' + properties[name] + '<br />';
                }
              } else {
                objListUpdate[objStore.variant_id] = 0;
              }

              html += '     <tr>';
              html += '       <td>';
              html += '         <center><img style="height: 150px;" src="' + objStore.image + '"</img>' + '</center>';
              html += '       </td>';
              html += '       <td>';
              html += '         <strong> <p class="restricted-msg">' + title + '</p>' + '</strong>' + '<br />';
              html +=           htmlProperties;
              html += '       </td>';
              html += '     </tr>';
              break;
            }
          }
        }
      });

      html += '   </tbody>';
      html += ' </table>';
      html += '</div>';
    }

    return {
      objUpdate: { updates: objListUpdate },
      arrUpdate: { updates: existsProperties ? arrListUpdate : []},
      html: hasContent ? html : ''
    }
  }

  function synchronizeCart(data) {
    if (data.responseText && data.responseText.length > 0) {
      var Carts = JSON.parse((data.responseText));
      var itemsStore = Carts.items,
          quantityProducts = 0;
  
      var loaderModal = new tingle.modal({
        footer: false,
        stickyFooter: false,
        closeMethods: []
      });

      loaderModal.setContent('<center><span> Wait a moment please </span><div style="padding-top: 10px;"><img src="https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderBlackBlue.gif"  /></div></center>');
      loaderModal.open();

      var restrictedModal = new tingle.modal({
        footer: true,
        stickyFooter: true,
        closeMethods: ['overlay', 'button', 'escape'],
        closeLabel: "Close",
        onClose: function() {
          if (quantityProducts === 0) {
            window.location.assign('/');
          }
        }
      });

      var prod = addProductItems(itemsStore);

      Sdk.addProductsCart(prod).then(function (response) {
        var productsResponse = response.Data ? response.Data.ListProducts : [];
        
        var arrayRemove = _.filter(productsResponse, function(prod) { 
          var success = prod.Success;
          return success === false; 
        });

        var currentCountry = sessionStore.get('auth-store').Data.CART_SKY.Country.Name,
            validate = null,
            html = '';

        if (arrayRemove.length > 0) {
          validate = validateRestriction(arrayRemove, itemsStore, currentCountry);
          html = validate.html;
          
          restrictedModal.setContent(html);
          restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function() {
              restrictedModal.close();
          }); 
        }

        Sdk.getCartRefresh().then(function (Cart) {
          quantityProducts = Cart ? Cart.Data.Cart.Items.length : 0;
          sessionStore.set('cart_prod_arr', Cart);

          var CartCustomField = {
            plataform: 'shopify',
            carts: Carts
          };

          Sdk.showCheckoutPageV2(CartCustomField);
          Sdk.Common().changeLastCountry(sessionStore.get('auth-store').Data.CART_SKY.Country.Name);         
          loaderModal.close(); 
            
          if (html.length > 0) {
            restrictedModal.open();
          }                  
        });
      });
    }
  }
});
