const path = require('path');
const webpack = require('webpack');
const conf = require('./config/store.json');
const CompressionPlugin = require('compression-webpack-plugin');

const plataforma = conf.MOD_PLATAFORM;
let entryPoint = '';

console.log('COMPILANDO A PLATAFORMA:::', plataforma);
if (plataforma.length > 0) {

  entryPoint = './src/entryPoint/' +  plataforma.toLocaleLowerCase().trim() + '.js';
  let nameFile = plataforma.toLocaleLowerCase().trim() + '.' + conf.IDSTORE;

  module.exports = {
    entry: {
      sky: entryPoint,
    },
    plugins: [      
      new webpack.ProvidePlugin({
        'jQuery': 'jquery',
        'window.jQuery': 'jquery',
        'jquery': 'jquery',
        'window.jquery': 'jquery',
        '$': 'jquery',
        'window.$': 'jquery',
        _:'underscore'
      }),
      new CompressionPlugin()
    ],
    module: {
      rules: [
        {
          use: {
            loader:'babel-loader',
            options: { presets: ['es2015'] }
          },
          test: /\.js$/,
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader'
          ]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            'file-loader'
          ]
        }
      ]
    },
    output: {
      filename: nameFile + '.dev.js',
      path: path.resolve(__dirname, 'dist')
    }
  };
}
else{
  console.log(':::INGRESE PLATAFORMA EN ARCHIVOS DE CONFIGURACION A PLATAFORMA:::');
}
